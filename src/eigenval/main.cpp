﻿#include <iostream>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>

// ハウスホルダー行列を取得
auto HouseholderMatrix(const boost::numeric::ublas::vector<double>& x)
{
	const auto n = x.size();

	const auto normX = boost::numeric::ublas::norm_2(x);

	auto v = x;
	v[0] += normX;
	const auto normV2 = boost::numeric::ublas::inner_prod(v, v);

	boost::numeric::ublas::matrix<double> H(n, n);
	H(0, 0) = 1 - 2 * v[0] * v[0] / normV2; H(0, 1) = 0 - 2 * v[0] * v[1] / normV2; H(0, 2) = 0 - 2 * v[0] * v[2] / normV2;
	H(1, 0) = 0 - 2 * v[1] * v[0] / normV2; H(1, 1) = 1 - 2 * v[1] * v[1] / normV2; H(1, 2) = 0 - 2 * v[1] * v[2] / normV2;
	H(2, 0) = 0 - 2 * v[2] * v[0] / normV2; H(2, 1) = 0 - 2 * v[2] * v[1] / normV2; H(2, 2) = 1 - 2 * v[2] * v[2] / normV2;

	return H;
}

int main()
{
	constexpr std::size_t DIM = 3;

	using Matrix = boost::numeric::ublas::matrix<double>;
	using Vector = boost::numeric::ublas::vector<double>;

	Vector x(DIM);
	x[0] = +0.1;
	x[1] = -0.5;
	x[2] = +1.85;

	const auto H = HouseholderMatrix(x);

	std::cout << H << std::endl;

	const Vector y = boost::numeric::ublas::prod(H, x);
	std::cout << y << ", " << boost::numeric::ublas::norm_2(y) << std::endl;

	return 0;
}
